#!/bin/bash

set -e

epochs="O1 O2"

for epoch in $epochs; do
    inis=`ls O1/*.ini 2>/dev/null || true`
    for ini in $inis; do
        echo -n "Validating $ini... "
        python tools/clf-to-omicron.py $ini -o test.ini 1> test.out
        echo "OK"
    done
done
